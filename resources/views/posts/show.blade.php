@extends('master')
@section('content')

<form >
@csrf
  <div class="form-group">
    <label for="TitleInput">Title</label>
    <input type="title" class="form-control" id="TitleInput" aria-describedby="titlehelp" placeholder="Title" name="title" value="{{$post->title}}" readonly size="32">
  </div>
  <div class="form-group">
    <label for="BodyInput">Body</label>
    <textarea class="form-control" id="BodyInput" placeholder="Body" rows="5" name="body" maxlength="65535" readonly>{{$post->body}}</textarea>
  </div>
</form>


@endsection