@extends('master')
@section('content')

<form action="{{url('/post/' . $post->id)}}" method="post">
@method('PUT')
@csrf
  <div class="form-group">
    <label for="TitleInput">Title</label>
    <input type="title" class="form-control" id="TitleInput" aria-describedby="titlehelp" placeholder="Title" name="title" value="{{$post->title}}" required size="32">
  </div>
  <div class="form-group">
    <label for="BodyInput">Body</label>
    <textarea class="form-control" id="BodyInput" placeholder="Body" rows="5" name="body" maxlength="65535" required>{{$post->body}}</textarea>
  </div>

  <button type="submit" class="btn btn-primary">Submit</button>
</form>


@endsection