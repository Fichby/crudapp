@extends('master')
@section('content')

<a class="btn btn-outline-success" href="{{route('post.create')}}">create</a>
<table class="table mt-2">
  <thead>
    <tr>
        <th scope="col">#</th>
        <th scope="col">Title</th>
        <th scope="col">Body</th>
        <th scope="col">Created</th>
        <th scope="col">Updated</th>
        <th scope="col">Action</th>
        <th scope="col">Show</th>
        <th scope="col">Delete</th>
    </tr>
  </thead>
  <tbody>
    @foreach($posts as $post)
        <tr>
            <td>{{$post->id}}</td>
            <td>{{$post->title}}</td>
            <td>{{$post->body}}</td>
            <td>{{$post->created_at}}</td>
            <td>{{$post->updated_at}}</td>
            <td><a class="btn btn-outline-primary" href="{{url('/post/'.$post->id .'/edit')}}">Edit</a></td>
            <td><a class="btn btn-outline-info" href="{{url('/post/'.$post->id )}}">Show</a></td>
            <td>
                <form action="{{url('/post/'.$post->id )}}" method="POST">
                    @csrf
                    @method('DELETE')
                    <button type="submit" class="btn btn-outline-danger" >Delete</button>
                </form>
            </td>
        </tr>
    @endforeach
  </tbody>
</table>
@endsection
