@component('mail::message')
# Post Created

A post has been created!

@component('mail::button', ['url' => 'http://localhost/post'])
View Post
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent