@component('mail::message')
# Post Updated

A post has been updated!

@component('mail::button', ['url' => 'http://localhost/post'])
View Post
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent