@include('partials.header')
@include('partials.nav')
<div class="container">
@if(count($errors) > 0)
    <div class="alert alert-warning alert-dismissible fade show" role="alert">
      <ul>
        @foreach($errors->all() as $error)
            <li>{{$error}}</li>
        @endforeach
      </ul>
      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>
@endif
@if(Session::has('success'))
    <div class="alert alert-success alert-dismissible fade show" role="alert">
      {{Session::get('success')}}
      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>
@endif
  <div class="row">
    <div class="col-2">

    </div>
    <div class="col-8">
      @yield('content')
    </div>
    <div class="col-2">

    </div>
  </div>
</div>

@include('partials.footer')
