# My Crud app

## Requirements

1. PHP 7.2
2. Composer

### Run the following to install

```
git clone https://Fichby@bitbucket.org/Fichby/crudapp.git
composer install
cp .env.example .env
```

### you will need to change the following setting in your env file

```
QUEUE_CONNECTION=database

MAIL_DRIVER=smtp
MAIL_HOST=somehost
MAIL_PORT=465
MAIL_USERNAME=someuser
MAIL_PASSWORD=somepassword
MAIL_ENCRYPTION=ssl
```
